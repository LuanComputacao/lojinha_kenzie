from faker import Faker
from hashlib import md5

fake = Faker()


def main():
    name = fake.name()
    name_hash = md5(name.encode())
    print(name, name_hash.hexdigest())
    
if __name__ == "__main__":
    main()
