from flask import Blueprint, request
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    jwt_refresh_token_required
)

from datetime import timedelta

from app.services.users_services import create_user
from app.models import User

bp_auth = Blueprint('auth', __name__, url_prefix='/auth')


@bp_auth.route('/signup', methods=['POST'])
def signup():
    name = request.json.get('name')
    surname = request.json.get('surname')
    document = request.json.get('document')

    response = create_user(name=name, surname=surname, document=document)

    return response


@bp_auth.route('/login', methods=['POST'])
def login():
    document = request.json.get('document')

    found_user = User.query.filter_by(document=document).first()
    access_token = create_access_token(identity=found_user.id, expires_delta=timedelta(days=7))
    fresh_token = create_refresh_token(identity=found_user.id, expires_delta=timedelta(days=14))

    return {'data': {
        'name': found_user.name,
        'surname': found_user.surname,
        'document': found_user.document,
        'access_token': access_token,
        'fresh_token': fresh_token
    }}


@bp_auth.route('/refresh', methods=['GET'])
@jwt_refresh_token_required
def refresh():
    user_id = get_jwt_identity()
    access_token = create_access_token(identity=user_id, expires_delta=timedelta(days=7))
    
    return {'access_token': access_token}
