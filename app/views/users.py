from flask import Blueprint, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from app.models import User, Address, db, UserSchema
from typing import List
from http import HTTPStatus
from sqlalchemy.exc import IntegrityError
from app.services.http import build_api_response
from app.services.users_services import serialize_user, serialize_user_list
from app.views.decorators.validators.api import require_json

bp_users = Blueprint('api_users', __name__, url_prefix='/users')


@bp_users.route('/')
@jwt_required
def list_all():
    users = User.query.all()

    user_id = get_jwt_identity()
    print(user_id)

    return {
        'data': UserSchema(many=True).dump(users)
    }, HTTPStatus.OK


@bp_users.route('/', methods=['POST'])
@require_json
def create():
    data = request.get_json()
    user = User(
        name=data["name"],
        surname=data['surname'],
        document=data['document']
    )

    try:
        db.session.add(user)
        db.session.commit()
        return build_api_response(HTTPStatus.CREATED)
    except IntegrityError:
        return build_api_response(HTTPStatus.BAD_REQUEST)


@bp_users.route('/get')
@jwt_required
def get():
    user_id = get_jwt_identity()
    user = User.query.get(user_id)
    if not user:
        return build_api_response(HTTPStatus.NOT_FOUND)

    return {'data': UserSchema().dump(user)}


@bp_users.route('/<int:user_id>', methods=['PUT'])
def put(user_id: int):
    data = request.get_json()

    user = User.query.get_or_404(user_id)

    user.document = data['document'] if data.get('document') else user.document
    user.name = data['name'] if data.get('name') else user.name
    user.surname = data['surname'] if data.get('surname') else user.surname
    adresses = Address.query.filter(Address.id.in_(data['adresses'])).all()

    if len(adresses) > 0:
        for address in adresses:
            user.adresses.append(address)
    else:
        user.adresses = []

    db.session.commit()
    return serialize_user(user)


@bp_users.route('/<int:user_id>', methods=['DELETE'])
def delete(user_id: int):
    User.query.filter_by(id = user_id).delete()
    db.session.commit()
    return build_api_response(HTTPStatus.OK)
