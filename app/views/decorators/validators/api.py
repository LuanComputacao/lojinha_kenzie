from functools import wraps
from flask import request
from app.services.http import build_api_response
from http import HTTPStatus

def require_json(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not request.get_json():
            return build_api_response(HTTPStatus.BAD_REQUEST)
        return f(*args, **kwargs)
    return decorated_function