from flask import Flask
from flask_jwt_extended import JWTManager
from secrets import token_hex
from flask_restful import Api
from environs import Env

from app.views.status import bp_status
from app.views.users import bp_users
from app.views.address import bp_adresses
from app.views.authentication import bp_auth
from app.models import db, mg, ma
from app.resources.user_resource import UserResource

def create_app():
    env = Env()
    env.read_env()
        
    app = Flask(__name__)
    api = Api()
    
    
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = env.bool('SQLALCHEMY_TRACK_MODIFICATIONS')
    app.config['SQLALCHEMY_DATABASE_URI'] = env.str('SQLALCHEMY_DATABASE_URI')
    app.config['JWT_SECRET_KEY'] = token_hex(16)
    
    JWTManager(app)
    db.init_app(app)
    mg.init_app(app, db)
    ma.init_app(app)


    app.register_blueprint(bp_status)
    app.register_blueprint(bp_users)
    app.register_blueprint(bp_adresses)
    app.register_blueprint(bp_auth)

    api.add_resource(UserResource, '/r-users', '/r-users/<int:id>')

    api.init_app(app)
    return app
