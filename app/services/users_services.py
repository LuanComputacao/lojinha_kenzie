from app.models import User
from typing import List
from app.models import db
from .http import build_api_response
from sqlalchemy.exc import IntegrityError
from http import HTTPStatus


def serialize_user(user: User) -> dict:
    return {
        'id': user.id,
        'name': user.name,
        'surname': user.surname,
        'document': user.document,
    }


def serialize_user_list(user_list: List[User]) -> List[dict]:
    return [serialize_user(user) for user in user_list]


def create_user(**kwargs):
    user = User(
        name=kwargs["name"],
        surname=kwargs['surname'],
        document=kwargs['document']
    )

    try:
        db.session.add(user)
        db.session.commit()
        return build_api_response(HTTPStatus.CREATED)
    except IntegrityError:
        return build_api_response(HTTPStatus.BAD_REQUEST)
