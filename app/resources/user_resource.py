from flask_restful import Resource, url_for
from app.models import User, UserSchema
from app.services.http import build_api_response
from http import HTTPStatus


class UserResource(Resource):
    def get(self, id=0):
        if id == 0:
            data = UserSchema(many=True).dump(
                User.query.all()
            )
        else:
            data = UserSchema().dump(
                User.query.get(id)
            )

        return {'data': data}

    def put(self, id):
        return {'data': 'deu boa também no put'}